#include "test.h"

#include <cmath>
#include <numbers>

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"

#include "dolphinmesh/meshio.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/primatives.hpp"

using namespace dolphinmesh;
using namespace linalg::num;

namespace test_primitives {

class Tetrahydron : public ::testing::Test {

public:
  TriMesh<double> mesh =

      TriMesh(load_obj("assets/tetrahydron.obj"), 2.0);

protected:
  void SetUp() override { mesh.verify_topology(); }
};

class TetrahydronWithBoundary : public ::testing::Test {

public:
  TriMesh<double> mesh =

      TriMesh(load_obj("assets/tetrahydron_with_boundary.obj"), 2.0);

protected:
  void SetUp() override { mesh.verify_topology(); }
};


TEST(primatives, types) {

  EXPECT_TRUE(typeid(Vertex<Float64>::value_type) == typeid(Float64));
  EXPECT_TRUE(typeid(Vertex<Float32>::value_type) == typeid(Float32));
  EXPECT_TRUE(typeid(Face<Float64>::value_type) == typeid(Float64));
  EXPECT_TRUE(typeid(Face<Float32>::value_type) == typeid(Float32));
  EXPECT_TRUE(typeid(Edge<Float64>::value_type) == typeid(Float64));
  EXPECT_TRUE(typeid(Edge<Float32>::value_type) == typeid(Float32));
  EXPECT_TRUE(typeid(Halfedge<Float64>::value_type) == typeid(Float64));
  EXPECT_TRUE(typeid(Halfedge<Float32>::value_type) == typeid(Float32));
}

TEST_F(Tetrahydron, comparisons) {

  auto he = mesh.halfedges[0];
  EXPECT_TRUE(he == he);
  EXPECT_FALSE(he != he);
  EXPECT_FALSE(he == *he.prev);
  EXPECT_TRUE(he != *he.prev);

  auto v = mesh.vertices[0];
  EXPECT_TRUE(v == v);
  EXPECT_FALSE(v != v);

  auto e = mesh.edges[0];
  EXPECT_TRUE(e == e);
  EXPECT_FALSE(e != e);

  auto f = mesh.faces[0];
  EXPECT_TRUE(f == f);
  EXPECT_FALSE(f != f);
}

TEST_F(Tetrahydron, halfedge_connectivity) {

  for (auto &he : mesh.halfedges) {
    EXPECT_TRUE(he.opposite->opposite == &he);
    EXPECT_TRUE(he.to_vertex() == he.opposite->from_vertex());
    EXPECT_TRUE(he.from_vertex() == he.opposite->to_vertex());
  }
}

TEST_F(TetrahydronWithBoundary, halfedge_orientation) {

  EXPECT_TRUE(mesh.halfedges[0].orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[1].orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[2].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[3].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[4].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[5].orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[6].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[7].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[8].orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[9].orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[10].orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[11].orientation() == -1);
}

TEST_F(TetrahydronWithBoundary, halfedge_dual_orientation) {

  EXPECT_TRUE(mesh.halfedges[0].dual_orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[1].dual_orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[2].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[3].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[4].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[5].dual_orientation() == 1);
  EXPECT_TRUE(mesh.halfedges[6].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[7].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[8].dual_orientation() == -1);
  EXPECT_TRUE(mesh.halfedges[9].dual_orientation() == 0);
  EXPECT_TRUE(mesh.halfedges[10].dual_orientation() == 0);
  EXPECT_TRUE(mesh.halfedges[11].dual_orientation() == 0);
}

TEST_F(TetrahydronWithBoundary, halfedge_opposite_angle) {

  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[0].opposite_angle(),
                                            std::numbers::pi / 4));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[1].opposite_angle(),
                                            std::numbers::pi / 2));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[2].opposite_angle(),
                                            std::numbers::pi / 4));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[3].opposite_angle(),
                                            std::numbers::pi / 4));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[4].opposite_angle(),
                                            std::numbers::pi / 4));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[5].opposite_angle(),
                                            std::numbers::pi / 2));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[6].opposite_angle(),
                                            std::numbers::pi / 3));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[7].opposite_angle(),
                                            std::numbers::pi / 3));
}

TEST_F(TetrahydronWithBoundary, halfedge_cotan_weight) {

  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[0].cotan_weight(), 1.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[1].cotan_weight(), 0.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[2].cotan_weight(), 1.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[3].cotan_weight(), 1.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[4].cotan_weight(), 1.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.halfedges[5].cotan_weight(), 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[6].cotan_weight(),
                                            1.0 / std::sqrt(3.0)));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.halfedges[7].cotan_weight(),
                                            1.0 / std::sqrt(3.0)));
}


TEST_F(Tetrahydron, edge_connectivity) {

  for (auto &edge : mesh.edges) {
    EXPECT_TRUE(edge.halfedge->edge == &edge);
    EXPECT_TRUE(edge.from_vertex() == edge.halfedge->from_vertex());
    EXPECT_TRUE(edge.to_vertex() == edge.halfedge->to_vertex());
  }
}

TEST_F(Tetrahydron, edge_length) {

  EXPECT_TRUE(linalg::num::within_tolerance(mesh.edges[0].length(), 1.0));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.edges[1].length(), std::sqrt(2.0)));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.edges[2].length(), std::sqrt(2.0)));
  EXPECT_TRUE(
      linalg::num::within_tolerance(mesh.edges[3].length(), std::sqrt(2.0)));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.edges[4].length(), 1.0));
  EXPECT_TRUE(linalg::num::within_tolerance(mesh.edges[5].length(), 1.0));
}

TEST_F(Tetrahydron, dual_edge_length) {

  for (auto &e : mesh.edges) {
    EXPECT_TRUE(e.length_dual_edge() > 0);
  }
}

TEST_F(TetrahydronWithBoundary, dual_edge_length) {

  for (auto &e : mesh.edges) {
    EXPECT_TRUE(e.length_dual_edge() > 0);
  }
}

TEST_F(Tetrahydron, edge_position) {

  for (auto &edge : mesh.edges) {
    auto p1 = edge.position(0.5);
    auto p2 = edge.center();
    // EXPECT_TRUE((edge.position(0.0) == edge.from_vertex()->position));
    // EXPECT_TRUE((edge.position(1.0) == edge.to_vertex()->position));
    EXPECT_TRUE(p1[0] == p2[0]);
    EXPECT_TRUE(p1[1] == p2[1]);
    EXPECT_TRUE(p1[2] == p2[2]);
  }
}

TEST_F(Tetrahydron, face_connectivity) {

  for (auto &f : mesh.faces) {
    EXPECT_TRUE(f.halfedge->face == &f);
    EXPECT_TRUE(f.halfedge == f.halfedge->prev->prev->prev);
  }
}

TEST_F(Tetrahydron, face_center) {
  auto face = mesh.faces[0];
  auto pt = face.center();
  EXPECT_TRUE(linalg::num::within_tolerance(pt[0], 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(pt[1], 1. / 3));
  EXPECT_TRUE(linalg::num::within_tolerance(pt[2], 1. / 3));
}

TEST_F(Tetrahydron, face_area) {
  auto &face = mesh.faces[0];
  EXPECT_TRUE(linalg::num::within_tolerance(face.area(), 0.5));
}

TEST_F(TetrahydronWithBoundary, face_normal_vector) {

  Vector3<double> n;

  n = mesh.faces[0].normal_direction();
  EXPECT_TRUE(linalg::num::within_tolerance(n[0], -1.0));
  EXPECT_TRUE(linalg::num::within_tolerance(n[1], 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(n[2], 0.0));

  n = mesh.faces[1].normal_direction();
  EXPECT_TRUE(linalg::num::within_tolerance(n[0], 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(n[1], 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(n[2], -1.0));

  n = mesh.faces[2].normal_direction();
  EXPECT_TRUE(linalg::num::within_tolerance(n[0], 1.0 / std::sqrt(3.0)));
  EXPECT_TRUE(linalg::num::within_tolerance(n[1], 1.0 / std::sqrt(3.0)));
  EXPECT_TRUE(linalg::num::within_tolerance(n[2], 1.0 / std::sqrt(3.0)));
}

TEST_F(TetrahydronWithBoundary, vertex_connectivity) {

  // check vertex->halfedge->from_vertex() connectivity
  for (auto &v : mesh.vertices) {
    EXPECT_TRUE(v.halfedge->from_vertex() == &v);
  }

  // check which vertices lie on a boundary
  EXPECT_TRUE(mesh.vertices[0].on_boundary());
  EXPECT_FALSE(mesh.vertices[1].on_boundary());
  EXPECT_TRUE(mesh.vertices[2].on_boundary());
  EXPECT_TRUE(mesh.vertices[3].on_boundary());

  // check boundary vertex -> halfedge connectivity
  EXPECT_TRUE(mesh.vertices[0].halfedge == &mesh.halfedges[4]);
  EXPECT_TRUE(mesh.vertices[2].halfedge == &mesh.halfedges[6]);
  EXPECT_TRUE(mesh.vertices[3].halfedge == &mesh.halfedges[2]);

  // check number of faces
  EXPECT_EQ(mesh.vertices[0].n_faces(), 2);
  EXPECT_EQ(mesh.vertices[1].n_faces(), 3);
  EXPECT_EQ(mesh.vertices[2].n_faces(), 2);
  EXPECT_EQ(mesh.vertices[3].n_faces(), 2);
}

TEST_F(Tetrahydron, get_vertex_position) {

  // check no copies are made during assignment and accessing
  auto &v = mesh.vertices[0];
  v.position[0] = 5;
  EXPECT_EQ(mesh.position[0][0], 5);
}

TEST_F(Tetrahydron, vertex_one_ring) {

  for (auto &v : mesh.vertices) {
    EXPECT_EQ(v.n_halfedges(), 3);
    EXPECT_EQ(v.n_edges(), 3);
    EXPECT_EQ(v.n_vertices(), 3);
    EXPECT_EQ(v.n_faces(), 3);
  }
}

TEST_F(TetrahydronWithBoundary, vertex_area_one_ring) {

  EXPECT_TRUE(within_tolerance(mesh.vertices[0].area_one_ring(),
                               0.5 + 0.5 * std::sqrt(3.0)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[1].area_one_ring(),
                               1.0 + 0.5 * std::sqrt(3.0)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[2].area_one_ring(),
                               0.5 + 0.5 * std::sqrt(3.0)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[3].area_one_ring(), 1.0));
}

TEST_F(TetrahydronWithBoundary, vertex_angle_sum) {

  EXPECT_TRUE(within_tolerance(mesh.vertices[0].angle_sum(),
                               std::numbers::pi * (7. / 12.)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[1].angle_sum(),
                               std::numbers::pi * (10. / 12.)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[2].angle_sum(),
                               std::numbers::pi * (7. / 12.)));
  EXPECT_TRUE(within_tolerance(mesh.vertices[3].angle_sum(),
                               std::numbers::pi));
}

TEST_F(TetrahydronWithBoundary, on_boundary) {

  for (auto h : mesh.boundaries[0].halfedges()) {
    EXPECT_TRUE(h->on_boundary());
    EXPECT_TRUE(h->edge->on_boundary());
    EXPECT_TRUE(h->from_vertex()->on_boundary());
  }
}

} // namespace test_primitives