# DolphinMesh

[![pipeline status](https://gitlab.com/dolphinmesh-ecosystem/core/dolphinmesh/badges/master/pipeline.svg)](https://gitlab.com/dolphinmesh-ecosystem/core/dolphinmesh/-/commits/master)
[![coverage report](https://gitlab.com/dolphinmesh-ecosystem/core/dolphinmesh/badges/master/coverage.svg)](https://dolphinmesh-ecosystem.gitlab.io/core/dolphinmesh/)

This library implements a [half-edge-mesh](https://en.wikipedia.org/wiki/Doubly_connected_edge_list) datastructure in C++20. A data-oriented design is used to maximize data-contiguity and thus maximize efficiency. The mesh data-structure has easy-to-use circulators around vertices, edges, halfedges and faces.

## Building and installing
The library has a header only design. Cmake is used in combination with [Conan](https://conan.io/) to automatically handle the depencies, which will be automatically downloaded, build and installed on the fly. If cmake and conan are installed on your system then you can simply build this project as follows
```
git clone https://gitlab.com/dolphinmesh-ecosystem/core/dolphinmesh.git
cd dolphinmesh
mkdir -p build && cd build
conan install ..
cmake .. -DBUILD_TESTING=ON
cmake --build .
ctest
```
