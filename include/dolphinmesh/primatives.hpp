#pragma once

#include <concepts>
#include <cmath>
#include <numbers>
#include <complex>

#include "dolphinmesh/generator.hpp"
#include "dolphinmesh/geometry.hpp"

#include "linalg/numbers.hpp"
#include "linalg/dense.hpp"
#include "linalg/affine.hpp"


namespace dolphinmesh {

using namespace cppcoro;
using namespace linalg;
using namespace linalg::num;
using namespace linalg::vector;
using namespace linalg::affine;

// forward declare to enable circular references
template <Real T> struct Edge;
template <Real T> struct Halfedge;
template <Real T> struct Vertex;
template <Real T> struct Face;

// Abstract type
template <Real T> struct MeshPrimative { using value_type = T; };

template <typename U>
concept mesh_primative = std::derived_from<U, MeshPrimative<typename U::value_type> >;

template<mesh_primative U>
using value_type = typename U::value_type;

template <mesh_primative V> bool operator==(const V &a, const V &b) {
  return &a == &b; // only true if addresses are the same
}

template <mesh_primative V> bool operator!=(const V &a, const V &b) {
  return &a != &b; // only true if addresses are the same
}

#pragma region Edge

template <Real T> struct Edge : public MeshPrimative<T> { 
public:

  using type = Edge;

  Halfedge<T> *halfedge = nullptr; 

  Vertex<T> *to_vertex() const { return halfedge->vertex;
  }

  Vertex<T> *from_vertex() const { return halfedge->opposite->vertex; }

  const bool on_boundary() const {
    return (halfedge->on_boundary() || halfedge->opposite->on_boundary());
  }

  // Return the cotangent weight for an edge.
  const T cotan_weight() const { 
      return 0.5 *
           (halfedge->cotan_weight() + halfedge->opposite->cotan_weight());
  }

  // return the edge length
  const T length() const { return halfedge->length();
  }

  // return the edge length
  const T length_dual_edge() const {
    auto l = T(0); 
    if (!halfedge->on_boundary())
      l += Fastor::norm(halfedge->face->center() - halfedge->center());

    if (!halfedge->opposite->on_boundary())
      l += Fastor::norm(halfedge->opposite->face->center() - halfedge->center());

    return l;
  }

  // return vector that corresponds to oriented edge
  const Vector3<T> to_vector() const { return halfedge->to_vector();
  }

  // compute the position as a function of barycentric coordinate lambda
  template <Real S> Point3<T> position(S lambda) const {
    return halfedge->position(lambda);
  }

  // compute the barycenter of this edge
  Point3<T> center() const { return halfedge->center();
  }

};

#pragma endregion


#pragma region Vertex

template <Real T> struct Vertex : public MeshPrimative<T> {
  
  using type = Vertex;

  // static variable used to initialize position incase non is provided
  inline static const Point3<T> origin{0,0,0};

  Fastor::TensorMap<T, 3> position;
  Halfedge<T> *halfedge = nullptr;

  Vertex() : position(origin.data()) {}

  Vertex(const Point3<T> &p) : position(p.data()) {}

  // check if the vertex lies on a boundary
  const bool on_boundary() const { return halfedge->opposite->on_boundary();
  }

  // determine number of edges emanating from a vertex
  const std::size_t n_halfedges() const { 

    std::size_t count = 0;
    for (const auto &he : halfedges()) {
      ++count;    
    }
    return count;
  }

  // determine number of edges emanating from a vertex
  const std::size_t n_edges() const { return n_halfedges();
  }

  // determine number of vertices emanating from a vertex
  const std::size_t n_vertices() const { return n_halfedges(); 
  }

  // determine number of vertices emanating from a vertex
  const std::size_t n_faces() const {
    std::size_t count = 0;
    for (const auto &he : faces()) {
      ++count;
    }
    return count;
  }

  // compute area in the one-ring
  const T area_one_ring() const {
    T area = 0;
    for (auto *face : faces()) {
      area += face->area();
    }
    return area;
  }

  // compute dual-cell area, which is 1/3 of the one-ring area
  const T area_dual_ring() const { return area_one_ring() / 3.0;
  }

  // compute cumulative angle around vertex
  const T angle_sum() const { 
    T angle = 0;
    for (auto* he : halfedges()) {
      angle += he->angle();
    }
    return angle;
  }

  // compute the Gaussian curvature measured at a vertex
  const T curvature() const {
    return (2 * std::numbers::pi - angle_sum()) / area_dual_ring();
  }

  // compute the averaged normal vector at this vertex
  const Vector3<T> normal_vector() const {
    Vector3<T> n;
    n.zeros();
    int count = 0;
    for (const auto &face : faces()) {
      n += face.area();
      ++count;
    }
    return n / count;
  }

  // compute the averaged normal direction at this vertex
  const Vector3<T> normal_direction() const {
    auto n = normal_vector();
    return n / Fastor::norm(n);
  }

  // construct an orthonormal frame at this vertex
  const geometry::Frame<T> orthonormal_frame() const {
    auto e_0 = halfedge->to_vector();
    e_0 /= Fastor::norm(e_0);
    auto e_2 = normal_direction();
    auto e_1 = Fastor::cross(e_2, e_0);
    return {e_0, e_1, e_2};
  }

  // Project Euclidean vector to the tangent space at the vertex
  std::complex<T> project_to_frame(const Vector3<T> &vector) const {
    geometry::Frame<T> frame = orthonormal_frame();
    T x = Fastor::inner(vector, frame.x);
    T y = Fastor::inner(vector, frame.y);
    return {x, y};
  }

  // iterate over all halfedges emanating from vertex
  generator<Halfedge<T>*> halfedges() const {
      auto current = halfedge;
      do {
        co_yield current;
        current = current->prev->opposite;
      
      } while (current != halfedge);
  }

  // iterate over all edges emanating from vertex
  generator<Edge<T> *> edges() const {
    auto current = halfedge;
    do {
      co_yield current->edge;
      current = current->prev->opposite;

    } while (current != halfedge);
  }

  // iterate over all faces in the one-ring of this vertex
  generator<Face<T> *> faces() const {
    auto current = halfedge;
    do {
      co_yield current->face;
      current = current->prev->opposite;

    } while (!current->on_boundary() && current != halfedge);
  }

  // iterate over all vertices in the one-ring of this vertex
  generator<Vertex<T> *> vertices() const {
    auto current = halfedge;
    do {
      co_yield current->vertex;
      current = current->prev->opposite;

    } while (current != halfedge);
  }

};

#pragma endregion


#pragma region Halfedge

template <Real T> struct Halfedge : public MeshPrimative<T> {

  using type = Halfedge;

  Vertex<T> *vertex = nullptr; // the vertex pointed to
  Edge<T> *edge = nullptr;
  Face<T> *face = nullptr;
  Halfedge<T> *prev = nullptr;
  Halfedge<T> *opposite = nullptr;

  Vertex<T> *to_vertex() const { return vertex; }

  Vertex<T> *from_vertex() const { return prev->vertex; }

  const bool is_opposite(Halfedge<T> *other) const {
    return ((other->from_vertex() == this->to_vertex()) &&
            (this->from_vertex() == other->to_vertex()));
  }

  const bool on_boundary() const { return face == nullptr; }

  // Return the orientation of the edge
  const int orientation() const { 
    if (this == this->edge->halfedge) {
      return 1;
    } 
    else {
      return -1;
    }
  }

  // return the orientation of the dual edge
  const int dual_orientation() const { 
    if (on_boundary()) {
      return 0;
    }
    else if (this == this->edge->halfedge) {
      return -1;
    } 
    else {
      return 1;
    }
  }

  // calculate the position of a point on the halfedge given its barycentric coordinates
  template<Real S> Point3<T> position(S lambda) const {
    return (1 - lambda) * from_vertex()->position + lambda * to_vertex()->position;
  }

  // calculate barycenter of edge
  const Point3<T> center() const { return position(0.5);
  }

  // from halfedge to vector
  const Vector3<T> to_vector() const {
    return to_vertex()->position - from_vertex()->position;
  }

  // return the length of the halfedge
  const T length() const { return Fastor::norm(to_vector()); }

  // return the cotangent of the opposite angle, or 0 if halfedge is on the boundary
  const T cotan_weight() const {
    assert(face->n_sides() == 3 &&
           "This function is only applicable to triangle meshes.");
    if (on_boundary())
      return T(0); 
    
    Vector3<T> a = prev->to_vector();
    Vector3<T> b = -prev->prev->to_vector();
    return Fastor::norm(Fastor::inner(a, b)) /
           Fastor::norm(Fastor::cross(a, b));
  }

  // Return the opposite angle, or 0 if this is an imaginary halfedge
  const T opposite_angle() const {
    assert(face->n_sides() == 3 &&
           "This function is only applicable to triangle meshes.");
    if (on_boundary())
      return 0;

    Vector3<T> a = prev->to_vector();
    Vector3<T> b = -prev->prev->to_vector();
    return geometry::angle(a, b);
  }

  // Return the angle between this halfedge and the previous one
  const T angle() const {
    if (on_boundary())
      return 0;

    Vector3<T> u = to_vector();
    Vector3<T> v = prev->opposite->to_vector();
    return geometry::angle(u, v);
  }

};

#pragma endregion


#pragma region Face

// ToDo: remove?
//std::array<int, 3> _face_vertices{0, 1, 2};


template <Real T> struct Face : public MeshPrimative<T> {
  
  using type = Face;

  Halfedge<T> *halfedge = nullptr;

  const int n_sides() const {
    int count = 0;
    for (auto &h : halfedges()) {
      ++count;
    }
    return count;
  }

  const Point3<T> center() const {
  
      // create new point
      auto pt = Point3<T>::zeros();
      
      // loop over vertices and average
      int n = 0;
      for (const auto *v : vertices()) {
        pt = pt + v->position;
        ++ n;
      }
      pt /= n;

      return pt;
  }

  // compute area of polygon face.
  const T area() const { 

    auto *he_1 = halfedge->prev;
    auto *he_2 = he_1->prev;
    
    auto& a = halfedge->vertex->position;
    T area = T(0);
    do {
        // get vertex positions
        auto &b = he_1->vertex->position;
        auto &c = he_2->vertex->position;

        // add triangle contribution to area
        area += 0.5 * Fastor::norm(Fastor::cross(b - a, c - a));

        // initialize next triangle
        he_1 = he_2;
        he_2 = he_2->prev;
    } while (he_2 != halfedge);

    return area;
  }

  // compute normal vector to this face
  const Vector3<T> normal_vector() const {
    auto *he_1 = halfedge->prev;
    auto *he_2 = he_1->prev;

    auto &a = halfedge->vertex->position;
    auto normal = Vector3<T>::zeros();
    int count = 0;
    do {
      // get vertex positions
      auto &b = he_1->vertex->position;
      auto &c = he_2->vertex->position;

      // add contribution to normal
      normal += Fastor::cross(c - a, b - a);

      // initialize next triangle
      count += 1;
      he_1 = he_2;
      he_2 = he_2->prev;
    } while (he_2 != halfedge);

    return normal / (2*count);
  }

  // normal direction
  const Vector3<T> normal_direction() const { 
    Vector3<T> n = normal_vector();
    return n / n.norm();
  }

  // construct an orthonormal frame on a face
  const geometry::Frame<T> orthonormal_frame() const { 
      auto e_0 = halfedge->to_vector(); e_0 /= Fastor::norm(e_0);
      auto e_2 = normal_direction();
      auto e_1 = Fastor::cross(e_2, e_0);
      return {e_0, e_1, e_2};
  }

  generator<Halfedge<T> *> halfedges() const {
    auto *he = halfedge;
    do {
      co_yield he;
      he = he->prev;
    } while (he != halfedge);
  }

  generator<Vertex<T> *> vertices() const {
    auto *he = halfedge;
    do {
      co_yield he->vertex;
      he = he->prev;
    } while (he != halfedge);
  }

  generator<Edge<T> *> edges() const {
    auto *he = halfedge;
    do {
      co_yield he->edge;
      he = he->prev;
    } while (he != halfedge);
  }
};

#pragma endregion


#pragma region Boundary

template<Real T> class Boundary {

private:

  Halfedge<T> *halfedge;
  int count_n_he = 0;
  bool is_init;

public :

  Boundary() = delete;

  Boundary(Halfedge<T> *first) : halfedge(first) { 
    assert(first->on_boundary());
    is_init = false;
  }

  const std::size_t n_halfedges() const { 
    if (is_init)
      return count_n_he;
    return count_n_halfedges(); 
  }

  Halfedge<T> *first() const { return halfedge; }

  generator<Halfedge<T>*> halfedges() const {
    auto *he = halfedge;
    do {
      co_yield he;
      he = he->prev;
    } while (he != halfedge);
  }

private:

  // count number of halfedges
  void count_n_halfedges() {
    auto* he = halfedge;
    count_n_he = 0;
    do {
      ++count_n_he;
      he = he->prev;
    } while (he != halfedge);
    is_init = true;
  }

};


#pragma endregion


} // namespace dolphinmesh