#pragma once

#include <unordered_set>
#include <vector>
#include <cmath>
#include <string>

#include "linalg/dense.hpp"
#include "linalg/fixed_capacity_array.hpp"
#include "linalg/numbers.hpp"
#include "dolphinmesh/facevertexmesh.hpp"
#include "dolphinmesh/primatives.hpp"
#include "dolphinmesh/generator.hpp"


using namespace linalg;
using namespace linalg::num;

namespace dolphinmesh {


    #pragma region Verify mesh

    struct mesh_topology_exception : public std::exception {

      std::string message;

      mesh_topology_exception(std::string message) : message(message) {}

      const char *what() const throw() { return message.c_str();
      }
    };

    void verify_mesh_condition(bool condition,
        std::string message = "The input mesh is not a valid half-edge-mesh.") {
      if (condition == false) {
        throw mesh_topology_exception(message);
      }
    }

    #pragma endregion


    #pragma region Mesh interface

    template<typename U>
    struct Tag {};

    template <typename T> struct crtp {
      T &underlying() { return static_cast<T &>(*this); }
      T const &underlying() const { return static_cast<T const &>(*this); }
    };

    template <typename Mesh, Real T> 
    class MeshInterface : public crtp<Mesh> {
        
    private:
        
        using V = Vertex<T>;
        using H = Halfedge<T>;
        using E = Edge<T>;
        using F = Face<T>;

        bool is_init = false;

    public:

      fixed_capacity_array<Point3<T>> position;
      fixed_capacity_array<Vertex<T>> vertices;
      fixed_capacity_array<Halfedge<T>> halfedges;
      fixed_capacity_array<Edge<T>> edges;
      fixed_capacity_array<Face<T>> faces;

      std::vector<Boundary<T>> boundaries{};
        
        using type = Mesh;
        using value_type = T;

        const bool is_initialized() const {
          return this->is_init;
        }

        const std::size_t n_vertices() const { return vertices.size(); }

        const std::size_t n_edges() const { return edges.size(); }

        const std::size_t n_halfedges() const { return halfedges.size(); }

        const std::size_t n_faces() const { return faces.size(); }

        const std::size_t n_boundary_components() const {
          return boundaries.size();
        }

        const bool is_closed() const { return n_boundary_components() == 0; }

        const std::size_t n_euler() const {
          return n_vertices() - n_edges() + n_faces();
        }

        const std::size_t n_genus() const {
          std::size_t double_g = 2 - n_euler() - n_boundary_components();
          assert((double_g % 2 == 0) && "Genus should be integer valued.");
          return double_g / 2;
        }

        const bool contains(const F &face) const {
          return ((&face >= faces.begin()) && (&face < faces.end()));
        }

        const bool contains(const V &vertex) const {
          return ((&vertex >= vertices.begin()) && (&vertex < vertices.end()));
        }

        const bool contains(const E &edge) const {
          return ((&edge >= edges.begin()) && (&edge < edges.end()));
        }

        const bool contains(const H &he) const {
          return ((&he >= halfedges.begin()) && (&he < halfedges.end()));
        }

        const std::size_t index(const V &vertex) const {
          assert(this->contains(vertex) &&
                 "This vertex is not part of the mesh. Check if unwanted "
                 "copies are made.");
          return (std::size_t)(&vertex - vertices.begin());
        }

        const std::size_t index(const E &edge) const {
          assert(this->contains(edge) && "This edge is not part of the mesh. "
                                         "Check if unwanted copies are made.");
          return (std::size_t)(&edge - edges.begin());
        }

        const std::size_t index(const F &face) const {
          assert(this->contains(face) && "This edge is not part of the mesh. "
                                         "Check if unwanted copies are made.");
          return (std::size_t)(&face - faces.begin());
        }

        const std::size_t index(const H &he) const {
          assert(this->contains(he) && "This edge is not part of the mesh. "
                                       "Check if unwanted copies are made.");
          return (std::size_t)(&he - halfedges.begin());
        }

        const bool verify_topology() const {
          bool c1 = check_halfedge_connectivity();
          bool c2 = check_edge_connectivity();
          bool c3 = check_face_connectivity();
          bool c4 = check_vertex_connectivity();
          bool c5 = check_mesh_normals();
          bool c6 = check_boundary_connectivity();
          return c1 && c2 && c3 && c4 && c5 && c6;
        }

        const T area() const {
          T area = 0;
          for (auto &face : faces) {
            area += face.area();
          }
          return area;
        }

        void add_vertex(const T &a, const T &b, const T &c) {
          add_vertex({a, b, c});
        }

        void split(Face<T> &face) {

          // check if the face is actually part of this mesh
          assert(this->contains(face) && "This face is not part of the mesh. "
                                         "Check if unwanted copies are made.");

          // insert and update position of center vertex
          Point3<T> &pts = position.push_back(Point3<T>(face.center()));
          Vertex<T> *v = &vertices.push_back(Vertex<T>(pts));

          // initialize halfedges and face
          Halfedge<T> *he_0 = face.halfedge;
          Halfedge<T> *he_1 = &halfedges.push_back(Halfedge<T>());
          Halfedge<T> *he_2 = &halfedges.push_back(Halfedge<T>());
          Face<T> *f = &face;

          // save previous halfedge
          Halfedge<T> *save_he_prev = he_0->prev;
          Halfedge<T> *save_he_1 = he_1;
          Halfedge<T> *save_he_last = he_2;

          // adjust the connections of all primitives on first face
          adjust_face_connections(f, he_0, he_1, he_2, v);

          // adjust connections on remaining faces
          do {

            he_0 = save_he_prev;
            he_1 = &halfedges.push_back(Halfedge<T>());
            he_2 = &halfedges.push_back(Halfedge<T>());
            f = &faces.push_back(Face<T>());

            // make connections along edges
            Edge<T> *e = &edges.push_back(Edge<T>());
            adjust_edge_connections(e, save_he_1, he_2);

            // save previous halfedge
            save_he_prev = he_0->prev;
            save_he_1 = he_1;

            // adjust the connections of all primitives on this face
            adjust_face_connections(f, he_0, he_1, he_2, v);

          } while (save_he_prev != face.halfedge);

          // make connections along last edge
          Edge<T> *e = &edges.push_back(Edge<T>());
          adjust_edge_connections(e, save_he_1, save_he_last);
        }

        const bool finalize() {

          is_init = false;

          // temporary queue
          std::unordered_set<Halfedge<T> *> queue;

          // set links for all halfedges, create edges
          for (auto &halfedge : this->halfedges) {

            auto *he = &halfedge;
            queue.insert(he);

            // check if he->opposite is in the queue and
            // update mesh topology
            for (auto &other : queue) {

              // if opposite is found ...
              if (he->is_opposite(other)) {

                // create new edge and add to collection
                auto *e = &this->edges.push_back(Edge<T>());

                // update edge --> halfedge connectivity
                e->halfedge = he;

                // update halfedge --> edge connectivity
                he->edge = e;
                other->edge = e;

                // update halfedge --> halfedge.opposite connectivity
                he->opposite = other;
                other->opposite = he;

                // initialize next edge
                queue.erase(he);
                queue.erase(other);
                break;
              }
            }
          }
          // at this point the `queue` only holds halfedges
          // that are associated with boundaries

          // next we create boundary halfedges, whose `opposite` are
          // in the queue, and necessary links are made.
          while (!queue.empty()) {

            // get first element of boundary component
            auto *he = *queue.begin();
            queue.erase(he);

            // treat first boundary halfedge
            auto *first = &this->halfedges.push_back(Halfedge<T>());
            first->opposite = he;
            he->opposite = first;
            auto *e = &edges.push_back(Edge<T>());
            e->halfedge = he;
            he->edge = e;
            first->edge = e;
            first->vertex = he->prev->vertex;
            // reset vertex->halfedge because we are on the boundary
            first->vertex->halfedge = he;

            // add first as the start of a new boundary component
            this->boundaries.push_back(Boundary(first)); 

            // loop over remaining halfedges of this boundary
            auto *next = he;
            auto *save = first;
            while (true) {

              while (he->prev->opposite != nullptr && !he->prev->opposite->on_boundary()) {
                he = he->prev->opposite;
              }

              // if halfedge does not yet exist, create it, set links
              if (he->prev->opposite == nullptr) {
                he = he->prev;
                next = &halfedges.push_back(Halfedge<T>());
                next->opposite = he;
                he->opposite = next;
                next->prev = save;
                e = &edges.push_back(Edge<T>());
                e->halfedge = he;
                he->edge = e;
                next->edge = e;
                next->vertex = he->prev->vertex;
                next->vertex->halfedge =
                    he; // reset vertex->halfedge because we are on the boundary

                // initialize next
                queue.erase(he);
                save = next;
              }
              // if boundary is traversed, close boundary chain and exit
              else if (he->prev->opposite == first) {
                first->prev = save;
                break;
              }
            }
          }

          // verify if a valid half-edge-mesh is produced
          if (verify_topology())
            is_init = true;

          return is_init;
        }

    protected:

      void initialize(const std::size_t n_faces, const std::size_t n_vertices,
                      const double storage_factor = 1.2) {

        assert((storage_factor >= 1 && storage_factor <= 2) && "Choose storage factor between 1 and 2.");

        // estimate number of edges and halfedges, which is complicated by
        // the genus and number of boundary components.
        // We use the Euler characteristic: chi = V - E + F = 2 -2g -b
        // E < V + F + 2g + b (we take as bound 2g+b = 10% of V+F)
        const std::size_t n_edges = std::lround(1.10 * (n_faces + n_vertices));
        const std::size_t n_halfedges = 2 * n_edges;

        // factor taking into account some additional memory, which allows edge
        // division and other refinement upto a limit
        position = fixed_capacity_array<Point3<T>>(
            std::lround(storage_factor * n_vertices));
        vertices = fixed_capacity_array<Vertex<T>>(
            std::lround(storage_factor * n_vertices));
        faces = fixed_capacity_array<Face<T>>(
            std::lround(storage_factor * n_faces));
        halfedges = fixed_capacity_array<Halfedge<T>>(
            std::lround(storage_factor * n_halfedges));
        edges = fixed_capacity_array<Edge<T>>(
            std::lround(storage_factor * n_edges));
      }

      void add_vertex(const Point3<T> &p) {
        position.push_back(p);
        vertices.push_back(position.back());
      }

      template <std::size_t N> 
      void add_face_imp(std::array<Vertex<T> *, N> v) {

        // add unitialized face
        faces.push_back(Face<T>());
        auto *face = &faces.back();

        // add unitialized halfedges
        auto *he = &halfedges.push_back(Halfedge<T>());
        he->face = face;
        auto *h = he;
        for (int i = 1; i < N; ++i) {
          h->prev = &halfedges.push_back(Halfedge<T>());
          h = h->prev;
        }

        // initialize halfedge vertex
        he->vertex = v[1];
        he->prev->vertex = v[0];
        h = he->prev;
        for (int i = N - 1; i > 1; --i) {
          h->prev->vertex = v[i];
          h = h->prev;
        }

        // initialize halfedge face
        h = he;
        for (int i = 0; i < N; ++i) {
          h->face = face;
          h = h->prev;
        }

        // initialize prev
        h = he;
        for (int i = 0; i < N - 1; ++i) {
          h = h->prev;
        }
        h->prev = he;

        // initialize face
        face->halfedge = he;

        // initialize vertices
        v[0]->halfedge = he;
        h = he;
        for (int i = N - 1; i > 0; --i) {
          h = h->prev;
          v[i]->halfedge = h;
        }
      }

      // initialize this object by moving from another object
      void move_from(Mesh &other) {
        this->position = std::move(other.position);
        this->vertices = std::move(other.vertices);
        this->faces = std::move(other.faces);
        this->edges = std::move(other.edges);
        this->halfedges = std::move(other.halfedges);
        this->boundaries = std::move(other.boundaries);
        this->is_init = true;
        other.is_init = false;
      }

      // initialize this object by copying from another object
      void copy_from(const Mesh &other) {

        this->initialize(other.faces.capacity(), other.vertices.capacity(),
                         1.0);

        for (const auto &p : other.position) {
          this->add_vertex(p);
        }

        for (const auto &face : other.faces) {
          this->underlying().copy_face(other, face);
        }

        this->finalize();
      }

      // copy face with n sides and add to mesh object
      void copy_face_with_3_sides(const Mesh &other, const Face<T> &face_to_copy) {
        auto *ha = face_to_copy.halfedge;
        auto *hb = ha->prev;
        auto *hc = hb->prev;
        this->underlying().add_face(other.index(*ha->vertex),
                                    other.index(*hb->vertex),
                                    other.index(*hc->vertex));
      }
      void copy_face_with_4_sides(const Mesh &other, const Face<T> &face_to_copy) {
        auto *ha = face_to_copy.halfedge;
        auto *hb = ha->prev;
        auto *hc = hb->prev;
        auto *hd = hc->prev;
        this->underlying().add_face(other.index(*ha->vertex), other.index(*hb->vertex),
                                    other.index(*hc->vertex), other.index(*hd->vertex));
      }
      void copy_face_with_5_sides(const Mesh &other, const Face<T> &face_to_copy) {
        auto *ha = face_to_copy.halfedge;
        auto *hb = ha->prev;
        auto *hc = hb->prev;
        auto *hd = hc->prev;
        auto *he = hd->prev;
        this->underlying().add_face(
            other.index(*ha->vertex), other.index(*hb->vertex),
            other.index(*hc->vertex), other.index(*hd->vertex), 
                      other.index(*he->vertex));
      }
      void copy_face_with_6_sides(const Mesh &other, const Face<T> &face_to_copy) {
        auto *ha = face_to_copy.halfedge;
        auto *hb = ha->prev;
        auto *hc = hb->prev;
        auto *hd = hc->prev;
        auto *he = hd->prev;
        auto *hf = he->prev;
        this->underlying().add_face(
            other.index(*ha->vertex), other.index(*hb->vertex),
            other.index(*hc->vertex), other.index(*hd->vertex),
            other.index(*he->vertex), other.index(*hf->vertex));
      }

      // verify vertex connectivity
      const bool check_vertex_connectivity() const {
        for (auto &v : vertices) {
          if (v.halfedge->from_vertex() != &v)
            return false;
        }
        return true;
      }
      
      // verify face connectivity 
      const bool check_face_connectivity() const {
        for (auto &f : faces) {
          if (f.halfedge->face != &f)
            return false;
        }
        return true;
      }
      
      // verify edge connectivity 
      const bool check_edge_connectivity() const {
        for (auto &edge : edges) {
          if (edge.halfedge->edge != &edge)
            return false;
        }
        return true;
      }

      // verify halfedge connectivity
      const bool check_halfedge_connectivity() const {

        for (auto &he : halfedges) {
          if (he.opposite->opposite != &he)
            return false;

          if (he.to_vertex() != he.opposite->from_vertex())
            return false;

          if (he.from_vertex() != he.opposite->to_vertex())
            return false;

          // check that all halfedges are connected as a loop, including
          // boundaries
          if (!he.on_boundary()) {
            auto *h = &he;
            int count = 0;
            do {
              h = h->prev;
              if (count > 10)
                return false;
            } while (h != &he);
          }
        }

        return true;
      }

      // verify that boundaries are loops
      const bool check_boundary_connectivity() const { 
        for (auto &b : boundaries) {
          int count = 0;
          for (auto &he : b.halfedges()) {
            if (count > 1e5) // value may be changed in the future
              return false;
            ++count;
          }
        }
        return true;
      }

      // verify that the face normals are consistent
      const bool check_mesh_normals() const {
        auto n_boundaries = boundaries.size();
        Halfedge<T> *he;
        if (n_boundaries > 1) {
          for (int j = 0; j < n_boundaries - 1; j++) {
            he = boundaries[j].first();
            for (int i = j + 1; i < n_boundaries; i++) {
              for (Halfedge<T>* h : boundaries[i].halfedges()) {
                bool inconsistent_normals =
                    (he->from_vertex() == h->from_vertex() &&
                     he->to_vertex() == h->to_vertex());
                if (inconsistent_normals) {
                  return false;
                }
              }
            }
          }
        }
        return true;
      }

      // ToDo: extend to polygon faces
      void adjust_edge_connections(Edge<T> *e, Halfedge<T> *he_1,
                                   Halfedge<T> *he_2) {
        e->halfedge = he_1;
        he_1->edge = e;
        he_2->edge = e;
        he_1->opposite = he_2;
        he_2->opposite = he_1;
      }

      // ToDo: extend to polygon faces
      void adjust_face_connections(Face<T> *f, Halfedge<T> *he_0,
                                   Halfedge<T> *he_1, Halfedge<T> *he_2,
                                   Vertex<T> *v) {

        he_0->prev = he_1;
        he_1->prev = he_2;
        he_2->prev = he_0;

        he_0->face = f;
        he_1->face = f;
        he_2->face = f;

        he_1->vertex = he_0->opposite->vertex;
        he_2->vertex = v;

        f->halfedge = he_0;

        v->halfedge = he_1;
      }

    }; // class MeshInterface


    #pragma endregion

} // namespace dolphinmesh