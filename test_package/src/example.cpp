#include <iostream>

#include "dolphinmesh/facevertexmesh.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/meshio.hpp"

using namespace dolphinmesh;

int main() {

  // build a mesh
  PolygonMesh<double> mesh{6, 9};
  mesh.add_vertex(0.0, 0.0, 0.0);  // 0
  mesh.add_vertex(1.0, 1.0, 0.0);  // 1
  mesh.add_vertex(2.0, 1.0, 0.0);  // 2
  mesh.add_vertex(3.0, 1.0, 0.0);  // 3
  mesh.add_vertex(4.0, 0.0, 0.0);  // 4
  mesh.add_vertex(3.0, -1.0, 0.0); // 5
  mesh.add_vertex(2.0, -1.0, 0.0); // 6
  mesh.add_vertex(1.0, -1.0, 0.0); // 7
  mesh.add_vertex(2.0, 0.0, 0.0);  // 8

  mesh.add_face(0, 7, 1);
  mesh.add_face(7, 8, 2, 1);
  mesh.add_face(7, 6, 8);
  mesh.add_face(6, 5, 2, 8);
  mesh.add_face(5, 3, 2);
  mesh.add_face(5, 4, 3);

  mesh.finalize();

  std::cout << "Halfedgemesh properties: " << std::endl;
  std::cout << "number of vertices: " << mesh.n_vertices() << std::endl;
  std::cout << "number of faces: " << mesh.n_faces() << std::endl;
  std::cout << "number of edges: " << mesh.n_edges() << std::endl;
  std::cout << "number of halfedges: " << mesh.n_halfedges() << std::endl;
  std::cout << "number of boundaries: " << mesh.n_boundary_components()
            << std::endl;
  std::cout << "mesh genus: " << mesh.n_genus() << std::endl;
  std::cout << std::endl << std::endl;

  // Load a mesh
  TriMesh<double> torus = TriMesh(load_obj("assets/torus.obj"), 1.2);

  std::cout << "Halfedgemesh Torus:" << std::endl;
  std::cout << "Initialized : " << torus.is_initialized() << std::endl;
  std::cout << "Number of vertices " << torus.n_vertices() << std::endl;
  std::cout << "Number of edges " << torus.n_edges() << std::endl;
  std::cout << "Number of faces " << torus.n_faces() << std::endl;
  std::cout << "Number of halfedges " << torus.n_halfedges() << std::endl;

  return 0;
}