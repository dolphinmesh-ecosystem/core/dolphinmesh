#pragma once

#include <vector>
#include <concepts>
#include "assert.h"

#include "linalg/numbers.hpp"
#include "primatives.hpp"
#include "generator.hpp"

namespace dolphinmesh {

    using namespace linalg::num;


    template<typename T>
    class Chain {

    private:
    
      std::vector<Halfedge<T> *> m_halfedges;
      bool forward_direction = true;

    public:

      Chain() = default;

      void set_forward_direction() { 
        assert(is_empty() && "Can only set direction before elements are added to Chain.");
        forward_direction = true; 
      }

      void set_backward_direction() {
        assert(is_empty() && "Can only set direction before elements are added to Chain.");
        forward_direction = false;
      }

      const bool is_empty() const { return m_halfedges.empty(); }

      const bool is_closed() const {
        if (m_halfedges.size() < 2)
          return false;

        if (forward_direction) {
          return m_halfedges.back()->to_vertex() ==
                 m_halfedges.front()->from_vertex();
        } else {
          return m_halfedges.back()->from_vertex() ==
                 m_halfedges.front()->to_vertex();
        }
      }

      const std::size_t n_vertices() const { 
        if (is_closed()) {
          return m_halfedges.size();
        }else {
          return m_halfedges.size() + 1;
        }
      }

      const std::size_t n_halfedges() const { return m_halfedges.size(); }

      void push_back(Halfedge<T>* he) {
        if (!is_empty()){
          if (forward_direction) {
            assert(m_halfedges.back()->to_vertex() == he->from_vertex() &&
                   "halfedges do not connect.");
          } else {
            assert(m_halfedges.back()->from_vertex() == he->to_vertex() &&
                   "halfedges do not connect.");
          }
        }
        
        // add new halfedge
        m_halfedges.push_back(he);
      }

      Halfedge<T> *front() const { return m_halfedges.front();
      }

      Halfedge<T> *back() const { return m_halfedges.back(); 
      }

      Halfedge<T>* get_halfedge(const std::size_t index) const {
        return m_halfedges[index];
      }

      Vertex<T> *get_vertex(const std::size_t index) const {
        // case: first vertex
        if (index == 0) {
          if (forward_direction) {
            return m_halfedges[0]->from_vertex();
          } else {
            return m_halfedges[0]->to_vertex();
          }
        }
        // case: general case
        else {
          if (forward_direction) {
            return m_halfedges[index]->to_vertex();
          } else {
            return m_halfedges[index]->from_vertex();
          }
        }
      }

      // traverse path
      cppcoro::generator<Halfedge<T> *> halfedges() const {
        for (auto *he : m_halfedges) {
          co_yield he;
        }
      }

      // traverse edges
      cppcoro::generator<Edge<T> *> edges() const {
        for (auto *he : m_halfedges) {
          co_yield he->edge;
        }
      }

      // traverse path from `start_vertex` to `current_root`
      cppcoro::generator<Vertex<T> *> vertices() const {
        if (forward_direction) {
          for (auto *he : m_halfedges) {
            co_yield he->to_vertex();
          }
        } else {
          for (auto *he : m_halfedges) {
            co_yield he->from_vertex();
          }
        }
      }

      // compute the length of the chain
      const T length() const { 
        T l = T(0);
        for (auto* he : m_halfedges) {
          l += he->length();
        }
        return l;
      }

    };

} // namespace dolphinmesh