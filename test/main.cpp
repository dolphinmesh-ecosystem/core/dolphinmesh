#include <iostream>
#include <vector>

#include "dolphinmesh/facevertexmesh.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/meshio.hpp"
#include "dolphinmesh/traits.hpp"

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"
#include "linalg/fixed_capacity_array.hpp"


using namespace dolphinmesh;
using namespace dolphinmesh::geometry;

using namespace linalg;
using namespace linalg::num;
using namespace linalg::vector;
using namespace linalg::affine;

int main() {

  // Load a mesh
  TriMesh<double> torus = TriMesh(load_obj("assets/torus-coarse-version.obj"), 1.2);

  std::cout << "Halfedgemesh Torus:" << std::endl;
  std::cout << "Initialized : " << torus.is_initialized() << std::endl;
  std::cout << "Number of vertices " << torus.n_vertices() << std::endl;
  std::cout << "Number of edges " << torus.n_edges() << std::endl;
  std::cout << "Number of faces " << torus.n_faces() << std::endl;
  std::cout << "Number of halfedges " << torus.n_halfedges() << std::endl;

  std::cout << "Mesh is closed: " << torus.is_closed() << std::endl;
  std::cout << "Genus: " << torus.n_genus() << std::endl;

  auto y = create_dynamic_traits<Vertex, linalg::fixed_capacity_array<double>>(torus);
  
  return 0;
}