#include "test.h"

#include <vector>

#include "dolphinmesh/facevertexmesh.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/meshio.hpp"
#include "dolphinmesh/traits.hpp"

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"
#include "linalg/fixed_capacity_array.hpp"

using namespace dolphinmesh;
using namespace linalg::num;

namespace test_traits {

class Tetrahydron : public ::testing::Test {

public:
  TriMesh<double> mesh =

      TriMesh(load_obj("assets/tetrahydron.obj"), 2.0);

protected:
  void SetUp() override { mesh.verify_topology(); }
};

TEST_F(Tetrahydron, vertex_traits_std_vector) {

  auto trait = create_dynamic_traits<Vertex, std::vector<int>>(mesh);

  trait[mesh.vertices[0]] = 1;
  trait[mesh.vertices[1]] = 2;
  trait[mesh.vertices[2]] = 3;
  trait[mesh.vertices[3]] = 4;

  EXPECT_EQ(trait[mesh.vertices[0]], 1);
  EXPECT_EQ(trait[mesh.vertices[1]], 2);
  EXPECT_EQ(trait[mesh.vertices[2]], 3);
  EXPECT_EQ(trait[mesh.vertices[3]], 4);

  EXPECT_EQ(trait.size(), mesh.n_vertices());
}

TEST_F(Tetrahydron, face_traits_std_vector) {

  auto trait = create_dynamic_traits<Face, std::vector<int>>(mesh);

  trait[mesh.faces[0]] = 1;
  trait[mesh.faces[1]] = 2;
  trait[mesh.faces[2]] = 3;
  trait[mesh.faces[3]] = 4;

  EXPECT_EQ(trait[mesh.faces[0]], 1);
  EXPECT_EQ(trait[mesh.faces[1]], 2);
  EXPECT_EQ(trait[mesh.faces[2]], 3);
  EXPECT_EQ(trait[mesh.faces[3]], 4);

  EXPECT_EQ(trait.size(), mesh.n_faces());
}

TEST_F(Tetrahydron, vertex_traits_fixed_size_vector) {
   
  auto trait = create_dynamic_traits<Vertex, fixed_capacity_array<int>>(mesh);

  trait[mesh.vertices[0]] = 1;
  trait[mesh.vertices[1]] = 2;
  trait[mesh.vertices[2]] = 3;
  trait[mesh.vertices[3]] = 4;

  EXPECT_EQ(trait[mesh.vertices[0]], 1);
  EXPECT_EQ(trait[mesh.vertices[1]], 2);
  EXPECT_EQ(trait[mesh.vertices[2]], 3);
  EXPECT_EQ(trait[mesh.vertices[3]], 4);

  EXPECT_EQ(trait.size(), mesh.n_vertices());
}


TEST_F(Tetrahydron, face_traits_fixed_size_vector) {

  auto trait = create_dynamic_traits<Face, fixed_capacity_array<int>>(mesh);

  trait[mesh.faces[0]] = 1;
  trait[mesh.faces[1]] = 2;
  trait[mesh.faces[2]] = 3;
  trait[mesh.faces[3]] = 4;

  EXPECT_EQ(trait[mesh.faces[0]], 1);
  EXPECT_EQ(trait[mesh.faces[1]], 2);
  EXPECT_EQ(trait[mesh.faces[2]], 3);
  EXPECT_EQ(trait[mesh.faces[3]], 4);

  EXPECT_EQ(trait.size(), mesh.n_faces());
}

} // namespace test_traits