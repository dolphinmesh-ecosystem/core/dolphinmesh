#include "test.h"

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"
#include "dolphinmesh/meshio.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/primatives.hpp"


using namespace dolphinmesh;
using namespace linalg::num;

namespace test_halfedgemesh {

#pragma region TriMesh 

class Tetrahydron : public ::testing::Test {

public:
  TriMesh<double> mesh = TriMesh(load_obj("assets/tetrahydron.obj"), 2.0);
  Point3<double> *ptr_to_p;
  Vertex<double> *ptr_to_v;
  Face<double> *ptr_to_f;
  Edge<double> *ptr_to_e;
  Halfedge<double> *ptr_to_h;

protected:
  void SetUp() override { 
      mesh.verify_topology(); 
      ptr_to_p = &mesh.position[0];
      ptr_to_v = &mesh.vertices[0];
      ptr_to_f = &mesh.faces[0];
      ptr_to_e = &mesh.edges[0];
      ptr_to_h = &mesh.halfedges[0];
  }
};

class TetrahydronWithBoundary : public ::testing::Test {

public:
  TriMesh<double> mesh =
      TriMesh(load_obj("assets/tetrahydron_with_boundary.obj"), 2.0);

protected:
  void SetUp() override { mesh.verify_topology(); }
};

TEST(Concepts, half_edge_mesh) {
  EXPECT_TRUE((Concept::HalfedgeMesh<TriMesh<double>>));
  EXPECT_TRUE((Concept::HalfedgeMesh<QuadMesh<double>>));
  EXPECT_TRUE((Concept::HalfedgeMesh<PolygonMesh<double>>));
}

TEST(MeshNormals, catch_inconsistent_mesh_normals) {
  TriMesh<double> mesh{4, 4};
  mesh.add_vertex(0.0, 0.0, 0.0); // 0
  mesh.add_vertex(1.0, 0.0, 0.0); // 1
  mesh.add_vertex(0.0, 1.0, 0.0); // 2
  mesh.add_vertex(0.0, 0.0, 1.0); // 3

  mesh.add_face(0, 1, 3);
  mesh.add_face(1, 2, 3);
  mesh.add_face(0, 3, 2);
  mesh.add_face(0, 1, 2);

  EXPECT_FALSE(mesh.finalize());
}

TEST_F(Tetrahydron, verify_mesh) {
  EXPECT_TRUE(mesh.verify_topology());
  EXPECT_TRUE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 2);

  EXPECT_EQ(mesh.n_vertices(), 4);
  EXPECT_EQ(mesh.n_edges(), 6);
  EXPECT_EQ(mesh.n_halfedges(), 12);
  EXPECT_EQ(mesh.n_faces(), 4);
  EXPECT_EQ(mesh.n_boundary_components(), 0);

  for (auto &f : mesh.faces) {
    EXPECT_EQ(f.n_sides(), 3);
  }
}

TEST_F(TetrahydronWithBoundary, verify_mesh) {
  EXPECT_TRUE(mesh.verify_topology());
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);

  EXPECT_EQ(mesh.n_vertices(), 4);
  EXPECT_EQ(mesh.n_edges(), 6);
  EXPECT_EQ(mesh.n_halfedges(), 12);
  EXPECT_EQ(mesh.n_faces(), 3);
  EXPECT_EQ(mesh.n_boundary_components(), 1);

  for (auto &f : mesh.faces) {
    EXPECT_EQ(f.n_sides(), 3);
  }
}

TEST_F(TetrahydronWithBoundary, boundary) {
  EXPECT_EQ(mesh.n_boundary_components(), 1);
  auto boundary = mesh.boundaries[0];

  // expect direct connectivity of boundary halfedges
  EXPECT_EQ(boundary.first(), boundary.first()->prev->prev->prev);

  // check the halfedges generator
  // expect three halfedges along the boundary
  int count = 0;
  for (const auto &he : boundary.halfedges()) {
    count += 1;
  }
  EXPECT_EQ(count, 3);
}

TEST_F(Tetrahydron, contains_face) {
  for (auto &face : mesh.faces) {
    EXPECT_TRUE(mesh.contains(face));
  }
}

TEST_F(Tetrahydron, contains_vertex) {
  for (auto &vertex : mesh.vertices) {
    EXPECT_TRUE(mesh.contains(vertex));
  }
}

TEST_F(Tetrahydron, contains_edge) {
  for (auto &edge : mesh.edges) {
    EXPECT_TRUE(mesh.contains(edge));
  }
}

TEST_F(Tetrahydron, contains_halfedge) {
  for (auto &he : mesh.halfedges) {
    EXPECT_TRUE(mesh.contains(he));
  }
}

TEST_F(Tetrahydron, vertex_index) {
  int index = 0;
  for (const auto &v : mesh.vertices) {
    EXPECT_EQ(mesh.index(v), index);
    index += 1;
  }
}

TEST_F(Tetrahydron, halfedge_index) {
  int index = 0;
  for (const auto &he : mesh.halfedges) {
    EXPECT_EQ(mesh.index(he), index);
    index += 1;
  }
}

TEST_F(Tetrahydron, edge_index) {
  int index = 0;
  for (const auto &e : mesh.edges) {
    EXPECT_EQ(mesh.index(e), index);
    index += 1;
  }
}

TEST_F(Tetrahydron, face_index) {
  int index = 0;
  for (const auto &f : mesh.faces) {
    EXPECT_EQ(mesh.index(f), index);
    index += 1;
  }
}

TEST_F(Tetrahydron, face_area) {
  int index = 0;
  EXPECT_TRUE(within_tolerance(mesh.area(), 1.5 + 0.5*std::sqrt(3.0)));
}

TEST_F(TetrahydronWithBoundary, face_area) {
  int index = 0;
  EXPECT_TRUE(within_tolerance(mesh.area(), 1.0 + 0.5*std::sqrt(3.0)));
}

TEST_F(Tetrahydron, face_split) {
  auto& face = mesh.faces[0];
  mesh.split(face);

  // check that mesh has the correct number of primitives after insertion
  EXPECT_EQ(mesh.n_vertices(), 5);
  EXPECT_EQ(mesh.n_edges(), 9);
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_halfedges(), 18);

  // check inserted vertex position
  auto& v = mesh.vertices.back();
  EXPECT_TRUE(linalg::num::within_tolerance(v.position[0], 0.0));
  EXPECT_TRUE(linalg::num::within_tolerance(v.position[1], 1. / 3.));
  EXPECT_TRUE(linalg::num::within_tolerance(v.position[2], 1. / 3.));

  // Check if the mesh topology is valid after insertion
  EXPECT_TRUE(mesh.verify_topology());
}

TEST_F(Tetrahydron, copy_constructor) {

  auto copy = TriMesh(mesh);

  EXPECT_FALSE(&copy.position[0] == ptr_to_p);
  EXPECT_FALSE(&copy.vertices[0] == ptr_to_v);
  EXPECT_FALSE(&copy.faces[0] == ptr_to_f);
  EXPECT_FALSE(&copy.edges[0] == ptr_to_e);
  EXPECT_FALSE(&copy.halfedges[0] == ptr_to_h);

  EXPECT_TRUE(copy.verify_topology());
  EXPECT_TRUE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 2);

  EXPECT_EQ(copy.n_vertices(), 4);
  EXPECT_EQ(copy.n_edges(), 6);
  EXPECT_EQ(copy.n_halfedges(), 12);
  EXPECT_EQ(copy.n_faces(), 4);
  EXPECT_EQ(copy.n_boundary_components(), 0);

  for (auto &f : copy.faces) {
    EXPECT_EQ(f.n_sides(), 3);
  }
}

TEST_F(Tetrahydron, move_constructor) {

  auto copy = TriMesh(std::move(mesh));

  EXPECT_FALSE(mesh.is_initialized() == true);
  EXPECT_TRUE(mesh.n_vertices()==0);
  EXPECT_TRUE(mesh.n_edges() == 0);
  EXPECT_TRUE(mesh.n_faces() == 0);
  EXPECT_TRUE(mesh.n_halfedges() == 0);

  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(&copy.position[0] == ptr_to_p);
  EXPECT_TRUE(&copy.vertices[0] == ptr_to_v);
  EXPECT_TRUE(&copy.faces[0] == ptr_to_f);
  EXPECT_TRUE(&copy.edges[0] == ptr_to_e);
  EXPECT_TRUE(&copy.halfedges[0] == ptr_to_h);
}

TEST_F(Tetrahydron, copy_assignment) {

  TriMesh<double> copy;
  copy = mesh;
  
  EXPECT_FALSE(&copy.position[0] == ptr_to_p);
  EXPECT_FALSE(&copy.vertices[0] == ptr_to_v);
  EXPECT_FALSE(&copy.faces[0] == ptr_to_f);
  EXPECT_FALSE(&copy.edges[0] == ptr_to_e);
  EXPECT_FALSE(&copy.halfedges[0] == ptr_to_h);

  EXPECT_TRUE(copy.verify_topology());
  EXPECT_TRUE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 2);

  EXPECT_EQ(copy.n_vertices(), 4);
  EXPECT_EQ(copy.n_edges(), 6);
  EXPECT_EQ(copy.n_halfedges(), 12);
  EXPECT_EQ(copy.n_faces(), 4);
  EXPECT_EQ(copy.n_boundary_components(), 0);

  for (auto &f : copy.faces) {
    EXPECT_EQ(f.n_sides(), 3);
  }
}

TEST_F(Tetrahydron, move_assignment) {

  TriMesh<double> copy;
  copy = std::move(mesh);

  EXPECT_FALSE(mesh.is_initialized() == true);
  EXPECT_TRUE(mesh.n_vertices() == 0);
  EXPECT_TRUE(mesh.n_edges() == 0);
  EXPECT_TRUE(mesh.n_faces() == 0);
  EXPECT_TRUE(mesh.n_halfedges() == 0);

  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(&copy.position[0] == ptr_to_p);
  EXPECT_TRUE(&copy.vertices[0] == ptr_to_v);
  EXPECT_TRUE(&copy.faces[0] == ptr_to_f);
  EXPECT_TRUE(&copy.edges[0] == ptr_to_e);
  EXPECT_TRUE(&copy.halfedges[0] == ptr_to_h);
}

#pragma endregion


#pragma region PolygonMesh

class PolyMesh : public ::testing::Test {

public:
  PolygonMesh<double> mesh{6, 9};
  Point3<double> *ptr_to_p;
  Vertex<double> *ptr_to_v;
  Face<double> *ptr_to_f;
  Edge<double> *ptr_to_e;
  Halfedge<double> *ptr_to_h;

protected:
  void SetUp() override {  

    mesh.add_vertex(0.0, 0.0, 0.0);  // 0
    mesh.add_vertex(1.0, 1.0, 0.0);  // 1
    mesh.add_vertex(2.0, 1.0, 0.0);  // 2
    mesh.add_vertex(3.0, 1.0, 0.0);  // 3
    mesh.add_vertex(4.0, 0.0, 0.0);  // 4
    mesh.add_vertex(3.0, -1.0, 0.0); // 5
    mesh.add_vertex(2.0, -1.0, 0.0); // 6
    mesh.add_vertex(1.0, -1.0, 0.0); // 7
    mesh.add_vertex(2.0, 0.0, 0.0);  // 8

    mesh.add_face(0, 7, 1);
    mesh.add_face(7, 8, 2, 1);
    mesh.add_face(7, 6, 8);
    mesh.add_face(6, 5, 2, 8);
    mesh.add_face(5, 3, 2);
    mesh.add_face(5, 4, 3);

    mesh.finalize();

    ptr_to_p = &mesh.position[0];
    ptr_to_v = &mesh.vertices[0];
    ptr_to_f = &mesh.faces[0];
    ptr_to_e = &mesh.edges[0];
    ptr_to_h = &mesh.halfedges[0];
  }
};

TEST_F(PolyMesh, properties) {
  EXPECT_TRUE(mesh.verify_topology());
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);

  EXPECT_EQ(mesh.n_vertices(), 9);
  EXPECT_EQ(mesh.n_edges(), 14);
  EXPECT_EQ(mesh.n_halfedges(), 28);
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_boundary_components(), 1);
}

TEST_F(PolyMesh, copy_constructor) {

  auto copy = PolygonMesh(mesh);

  EXPECT_FALSE(&copy.position[0] == ptr_to_p);
  EXPECT_FALSE(&copy.vertices[0] == ptr_to_v);
  EXPECT_FALSE(&copy.faces[0] == ptr_to_f);
  EXPECT_FALSE(&copy.edges[0] == ptr_to_e);
  EXPECT_FALSE(&copy.halfedges[0] == ptr_to_h);

  EXPECT_TRUE(mesh.is_initialized() == true);
  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(copy.verify_topology());
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);
  
  EXPECT_EQ(copy.n_vertices(), 9);
  EXPECT_EQ(copy.n_edges(), 14);
  EXPECT_EQ(copy.n_halfedges(), 28);
  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(PolyMesh, copy_assignment) {
  
  PolygonMesh<double> copy;
  copy = mesh;

  EXPECT_FALSE(&copy.position[0] == ptr_to_p);
  EXPECT_FALSE(&copy.vertices[0] == ptr_to_v);
  EXPECT_FALSE(&copy.faces[0] == ptr_to_f);
  EXPECT_FALSE(&copy.edges[0] == ptr_to_e);
  EXPECT_FALSE(&copy.halfedges[0] == ptr_to_h);

  EXPECT_TRUE(mesh.is_initialized() == true);
  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(copy.verify_topology());
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);

  EXPECT_EQ(copy.n_vertices(), 9);
  EXPECT_EQ(copy.n_edges(), 14);
  EXPECT_EQ(copy.n_halfedges(), 28);
  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(PolyMesh, move_constructor) {

  auto copy = PolygonMesh(std::move(mesh));

  EXPECT_FALSE(mesh.is_initialized() == true);
  EXPECT_TRUE(mesh.n_vertices() == 0);
  EXPECT_TRUE(mesh.n_edges() == 0);
  EXPECT_TRUE(mesh.n_faces() == 0);
  EXPECT_TRUE(mesh.n_halfedges() == 0);

  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(&copy.position[0] == ptr_to_p);
  EXPECT_TRUE(&copy.vertices[0] == ptr_to_v);
  EXPECT_TRUE(&copy.faces[0] == ptr_to_f);
  EXPECT_TRUE(&copy.edges[0] == ptr_to_e);
  EXPECT_TRUE(&copy.halfedges[0] == ptr_to_h);
}

TEST_F(PolyMesh, move_assignment) {

  PolygonMesh<double> copy;
  copy = std::move(mesh);

  EXPECT_FALSE(mesh.is_initialized() == true);
  EXPECT_TRUE(mesh.n_vertices() == 0);
  EXPECT_TRUE(mesh.n_edges() == 0);
  EXPECT_TRUE(mesh.n_faces() == 0);
  EXPECT_TRUE(mesh.n_halfedges() == 0);

  EXPECT_TRUE(copy.is_initialized() == true);
  EXPECT_TRUE(&copy.position[0] == ptr_to_p);
  EXPECT_TRUE(&copy.vertices[0] == ptr_to_v);
  EXPECT_TRUE(&copy.faces[0] == ptr_to_f);
  EXPECT_TRUE(&copy.edges[0] == ptr_to_e);
  EXPECT_TRUE(&copy.halfedges[0] == ptr_to_h);
}

#pragma endregion


} // namespace test_halfedgemesh