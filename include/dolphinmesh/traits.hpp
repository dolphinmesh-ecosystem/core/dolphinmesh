#pragma once

#include <cmath>
#include <string>
#include <vector>

#include "halfedgemesh.hpp"
#include "linalg/numbers.hpp"

using namespace linalg;
using namespace linalg::num;

namespace dolphinmesh {

namespace Concept {

template <typename A>
concept ArrayInterface = requires(A a, std::size_t index) {
  std::regular<typename A::value_type>;
  { a[index] } -> std::same_as<typename A::value_type &>;
};

} // namespace Concept


template <typename Array> Array allocate(const std::size_t &n);


// allocate a standard vector
template <typename Array>
    requires std::same_as< Array,std::vector <typename Array::value_type>> 
Array allocate(const std::size_t& n) {
  using T = Array::value_type;
  return {std::vector<T>(n)};
}

// allocate a linalg::fixed_capacity_array
template <typename Array>
    requires std::same_as<Array, linalg::fixed_capacity_array<typename Array::value_type>>
Array allocate(const std::size_t &n) {
  using T = Array::value_type;
  auto v = linalg::fixed_capacity_array<T>(n);
  v.initialize();
  return {v};
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="Mesh"></typeparam>
/// <typeparam name="Array"></typeparam>
template <template <class> class V, Concept::HalfedgeMesh Mesh,
          Concept::ArrayInterface Array>
class DynamicTraits {

  using S = typename Array::value_type;
  using T = typename Mesh::value_type;

  const Mesh &m_mesh;
  Array m_data;

public:

  DynamicTraits(const Mesh &mesh)
      : m_mesh(mesh), m_data{ allocate<Array>(size()) } {
  }

  DynamicTraits(const Mesh &mesh, const Array &data) : m_mesh(mesh), m_data(data) {
    assert(data.size() == size() &&
           "Data size is not consistent with number of mesh primatives.");
  }

  const S &operator[](const V<T> &v) const { 
    auto z = m_mesh.index(v);
    auto x = m_data[m_mesh.index(v)];
    return m_data[m_mesh.index(v)]; 
  }

  S &operator[](const V<T> &v) {
    auto z = m_mesh.index(v);
    auto x = m_data[m_mesh.index(v)];
    return m_data[m_mesh.index(v)]; 
  }

  const std::size_t size() const {

    if constexpr (std::is_same<V<T>, Vertex<T>>::value) {
      return m_mesh.n_vertices();
    }
    if constexpr (std::is_same<V<T>, Edge<T>>::value) {
      return m_mesh.n_edges();
    }
    if constexpr (std::is_same<V<T>, Halfedge<T>>::value) {
      return m_mesh.n_halfedges();
    }
    if constexpr (std::is_same<V<T>, Face<T>>::value) {
      return m_mesh.n_faces();
    }
  }
};

template <template <class> class V, 
    Concept::ArrayInterface Array, Concept::HalfedgeMesh Mesh>
DynamicTraits<V, Mesh, Array> create_dynamic_traits(const Mesh &mesh,
                                                    const Array &data) {
  return {mesh, data};
}

template <template <class> class V,
          Concept::ArrayInterface Array, Concept::HalfedgeMesh Mesh>
DynamicTraits<V, Mesh, Array> create_dynamic_traits(const Mesh &mesh) {
  return {mesh};
}
} // namespace dolphinmesh