#include "test.h"
#include <cmath>

#include "dolphinmesh/meshio.hpp"
#include "dolphinmesh/halfedgemesh.hpp"
#include "dolphinmesh/primatives.hpp"
#include "dolphinmesh/chain.hpp"


using namespace dolphinmesh;

namespace test_chain {

TEST(chain, boundary) {

  PolygonMesh<double> mesh{6, 9};

  mesh.add_vertex(0.0, 0.0, 0.0);  // 0
  mesh.add_vertex(1.0, 1.0, 0.0);  // 1
  mesh.add_vertex(2.0, 1.0, 0.0);  // 2
  mesh.add_vertex(3.0, 1.0, 0.0);  // 3
  mesh.add_vertex(4.0, 0.0, 0.0);  // 4
  mesh.add_vertex(3.0, -1.0, 0.0); // 5
  mesh.add_vertex(2.0, -1.0, 0.0); // 6
  mesh.add_vertex(1.0, -1.0, 0.0); // 7
  mesh.add_vertex(2.0, 0.0, 0.0);  // 8

  mesh.add_face(0, 7, 1);
  mesh.add_face(7, 8, 2, 1);
  mesh.add_face(7, 6, 8);
  mesh.add_face(6, 5, 2, 8);
  mesh.add_face(5, 3, 2);
  mesh.add_face(5, 4, 3);

  mesh.finalize();


  EXPECT_TRUE(mesh.n_boundary_components() == 1);

  auto boundary = mesh.boundaries[0];
  Chain<double> c;
  c.set_backward_direction();
  for (Halfedge<double> *he : boundary.halfedges()) {
    c.push_back(he);
  }

  EXPECT_TRUE(c.is_closed());
  EXPECT_TRUE(c.n_halfedges() == 8);
  EXPECT_TRUE(c.n_vertices() == 8);
  EXPECT_TRUE(std::abs(c.length() - 4 * (1.0 + std::sqrt(2.0))) < 1e-12);
}

} // namespace test_halfedgemesh