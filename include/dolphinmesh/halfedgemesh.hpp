#pragma once

#include <unordered_set>
#include <vector>
#include <cmath>
#include <string>

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"
#include "linalg/fixed_capacity_array.hpp"

#include "meshinterface.hpp"
#include "facevertexmesh.hpp"
#include "primatives.hpp"
#include "generator.hpp"

namespace dolphinmesh {

    using namespace linalg;
    using namespace linalg::num;

    
    namespace Concept {

        template <typename Mesh>
        concept HalfedgeMesh =
            std::derived_from<Mesh, MeshInterface<Mesh, typename Mesh::value_type>>;
    } // namespace Concept


    template <typename T> 
    class PolygonMesh : public MeshInterface<PolygonMesh<T>,T> {

        friend class MeshInterface<PolygonMesh<T>, T>;

    public:

      PolygonMesh() = default;

      PolygonMesh(const std::size_t n_faces, const std::size_t n_vertices,
                  const double storage_factor = 1.2) {
        this->initialize(n_faces, n_vertices, storage_factor);
      }

      // move constructor
      PolygonMesh(PolygonMesh<T>&& other) noexcept{ 
          this->move_from(other);
      }

      // copy constructor
      PolygonMesh(const PolygonMesh<T>& other) { 
        this->copy_from(other);
      }

      // copy assignment operator
      PolygonMesh<T>& operator=(const PolygonMesh<T>& other) { 
        this->copy_from(other);
        return *this;
      }

      // move assignment operator
      PolygonMesh<T> &operator=(PolygonMesh<T> &&other) noexcept {
        this->move_from(other);
        return *this;
      }

      // destructor
      ~PolygonMesh() = default;

      template <Integer... Ints>
      void add_face(const std::size_t &i_a, const std::size_t &i_b,
                    const std::size_t &i_c, const Ints &&...i_d) {
        using Array = std::array<Vertex<T>*, 3+sizeof...(i_d)>;
        Array face_vertices{&this->vertices[i_a], &this->vertices[i_b],
                            &this->vertices[i_c], &this->vertices[std::forward<const Ints>(i_d)]...};
        return this->add_face_imp(face_vertices);
      }

    protected:
      
      void copy_face(const PolygonMesh<T>& other, const Face<T> &face_to_copy) {
        const int n = face_to_copy.n_sides();
        switch (n) {
        case 3:
          this->copy_face_with_3_sides(other, face_to_copy);
          break;
        case 4:
          this->copy_face_with_4_sides(other, face_to_copy);
          break;
        case 5:
          this->copy_face_with_5_sides(other, face_to_copy);
          break;
        case 6:
          this->copy_face_with_6_sides(other, face_to_copy);
          break;
        default:
          throw "Faces with 3-6 sides are supported.";
        }
      }

    };


    template <typename T>
    class TriMesh : public MeshInterface<TriMesh<T>, T> {

      friend class MeshInterface<TriMesh<T>, T>;

    public:

      TriMesh() = default;

      TriMesh(const std::size_t n_faces, const std::size_t n_vertices,
                  const double storage_factor = 1.2) {
        this->initialize(n_faces, n_vertices, storage_factor);
      }

      TriMesh(const face_vertex_mesh<T> &other,
                  const double storage_factor = 1.2) {
        this->initialize(other.faces.size(), other.vertices.size(),
                         storage_factor);
        this->add_vertices(other);
        this->add_faces(other);
        this->finalize();
      }

      // Move constructor
      TriMesh(TriMesh<T>&& other) { 
        this->move_from(other);
      }

      // Copy constructor
      TriMesh(const TriMesh<T> &other) { 
        this->copy_from(other); 
      }

      // copy assignment operator
      TriMesh<T> &operator=(const TriMesh<T> &other) {
        this->copy_from(other);
        return *this;
      }

      // move assignment operator
      TriMesh<T>& operator=(TriMesh<T>&& other) {   
        this->move_from(other);
        return *this;
      }

      // destructor
      ~TriMesh() = default;

      void add_face(const std::size_t &i_a, const std::size_t &i_b,
                    const std::size_t &i_c) {
        this->add_face_imp(std::array<Vertex<T> *, 3>{
            &this->vertices[i_a], &this->vertices[i_b], &this->vertices[i_c]});
      }

    protected:

      void add_vertices(const face_vertex_mesh<T> &other) {
        for (const auto &p : other.vertices) {
          this->add_vertex(p);
        }
      }

      void add_faces(const face_vertex_mesh<T> &other) {
        for (const auto &[ia, ib, ic] : other.faces) {
          add_face(ia, ib, ic);
        }
      }

      void copy_face(const TriMesh<T> &other, const Face<T> &face_to_copy) {
        this->copy_face_with_3_sides(other, face_to_copy);
      }

    };


    template <typename T>
    class QuadMesh : public MeshInterface<QuadMesh<T>, T> {

        friend class MeshInterface<QuadMesh<T>, T>;

    public:

      QuadMesh() = default;

      QuadMesh(const std::size_t n_faces, const std::size_t n_vertices,
                  const double storage_factor = 1.2) {
        this->initialize(n_faces, n_vertices, storage_factor);
      }

      // Move constructor
      QuadMesh(QuadMesh<T> &&other) {
        this->move_from(other);
      }

      // Copy constructor
      QuadMesh(const QuadMesh<T> &other) { this->copy_from(other); }

      // copy assignment operator
      QuadMesh<T> &operator=(const QuadMesh<T> &other) {
        this->copy_from(other);
        return *this;
      }

      // move assignment operator
      QuadMesh<T> &operator=(QuadMesh<T> &&other) {
        this->move_from(other);
        return *this;
      }

      // destructor
      ~QuadMesh() = default;

      void add_face(const std::size_t &i_a, const std::size_t &i_b,
                    const std::size_t &i_c, const std::size_t &i_d) {
        this->add_face_imp(std::array<Vertex<T> *, 4>{
            &this->vertices[i_a], &this->vertices[i_b],
            &this->vertices[i_c], &this->vertices[i_d]});
      }

      // iterate over all vertices in the one-ring of this vertex
      generator<Vertex<T> &> extraordinary_vertices() const {

        for (auto &vertex : this.vertices) {
          // boundary vertex
          if (vertex.on_boundary()) {
            if (vertex.n_edges() != 3) {
              co_yield vertex;
            }
          }
          // interior vertex
          else {
            if (vertex.n_edges() != 4) {
              co_yield vertex;
            }
          }
        }
      }

    protected:
      
      void copy_face(const QuadMesh<T> &other, const Face<T> &face_to_copy) {
        this->copy_face_with_4_sides(other, face_to_copy);
      }
      
    };


} // namespace dolphinmesh