#pragma once

#include <cmath>
#include <complex>
#include <concepts>
#include <numbers>

#include "linalg/dense.hpp"
#include "linalg/numbers.hpp"
#include "linalg/affine.hpp"


namespace dolphinmesh::geometry {

using namespace dolphinmesh;
using namespace linalg;
using namespace linalg::num;
using namespace linalg::vector;
using namespace linalg::affine;

/// <summary>
/// Definition of a 3D frame
/// </summary>
template <Real T> struct Frame {
  Vector3<T> x;
  Vector3<T> y;
  Vector3<T> z;

  Frame(const Vector3<T> &x, const Vector3<T> &y, const Vector3<T> &z)
      : x(x), y(y), z(z) {}
};

/// <summary>
/// Rotation matrix orresponding to an input angle 'theta'
/// </summary>
/// <param name="theta"></param>
/// <returns></returns>
template <Real T> Matrix2<T> rotation(const T &theta) {
  Matrix2<T> R;
  R.zeros();
  R(0, 0) = std::cos(theta);
  R(0, 1) = -std::sin(theta);
  R(1, 0) = std::sin(theta);
  R(1, 1) = std::cos(theta);
}

/// <summary>
/// Rotation using a complex representation
/// </summary>
/// <param name="theta"></param>
/// <returns></returns>
template <Real T> std::complex<T> complex_rotation(const T &theta) {
  return std::complex<T>(std::cos(theta), std::sin(theta));
}

/// <summary>
/// Compute angle between two vectors
/// </summary>
/// <param name="u"></param>
/// <param name="v"></param>
/// <returns></returns>
template <Real T> T angle(const Vector3<T> &u, const Vector3<T> &v) {
  return std::acos(std::max(
      -1.0,
      std::min(1.0, Fastor::inner(u / Fastor::norm(u), v / Fastor::norm(v)))));
}


} // namespace dolphinmesh::geometry
