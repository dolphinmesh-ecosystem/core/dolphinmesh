#pragma once

#include <iostream>
#include <fstream>
#include <sstream>

#include "dolphinmesh/primatives.hpp"
#include "dolphinmesh/facevertexmesh.hpp"

namespace dolphinmesh {


struct file_access_error : public std::exception {

  std::string message;

  file_access_error(std::string message) : message(message) {}

  const char *what() const throw() { return message.c_str(); }
};

face_vertex_mesh<double> load_obj(std::string file_name) {

  // file to read from
  std::ifstream file(file_name);

  // create a vertex-face mesh
  auto mesh = face_vertex_mesh<double>();

  // declare variables
  std::string line;
  char first_character;
  double x, y, z;
  int a, b, c;

  if(file.is_open()) {
    // read file line by line
    while (std::getline(file, line)) {

      std::istringstream iss(line); // read line into buffer
      iss >> first_character;       // take the first character

      switch (first_character) {
      case 'v':
        iss >> x >> y >> z;
        mesh.add_vertex({x, y, z});
        break;

      case 'f':
        iss >> a >> b >> c;
        mesh.add_face(a-1, b-1, c-1); // we use zero-indexing
        break;
      }
    }
  } else {
    std::cout << "WARNING: File could not be found." << std::endl;
    //throw file_access_error("File could not be found.");
  }
  
  return mesh;
}

} // namespace dolphinmesh
