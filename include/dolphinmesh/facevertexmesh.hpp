#pragma once

#include <vector>

#include "linalg/numbers.hpp"
#include "dolphinmesh/primatives.hpp"

using namespace linalg::num;

namespace dolphinmesh {

struct face_vertex_indices {
  int a;
  int b;
  int c;
};

template <Real T> class face_vertex_mesh {
    
public:

  std::vector<Point3<T>> vertices;
  std::vector<face_vertex_indices> faces{};

  face_vertex_mesh() {}

  void add_vertex(const Point3<T> &new_point) { vertices.push_back(new_point); }

  void add_face(int a, int b, int c) {
    faces.push_back(face_vertex_indices{a, b, c});
  }
};

} // namespace dolphinmesh